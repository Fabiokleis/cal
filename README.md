# calr

A calendar cli, simply show calendar

## Description

A calendar cli written in [Rust](https://doc.rust-lang.org/). It's software that was created for the purpose of studying more about Rust.

## Installation

## Usage

```console
cargo run
```

## Contributing

Simply open an issue if you find an error or open a PR if you want to add new features.

## Authors 

Fabio kleis

## License

MIT
